import React, {useState} from 'react'
import {View, TextInput, Button, StyleSheet, Modal} from 'react-native'

const GoalInput = (props) => {
    const [enteredGoal, setEnteredGoal] = useState('');

    const goalInputHandler = (text) => {
        setEnteredGoal(text)
      };

    const addGoalHandler = () => {
        props.addGoal(enteredGoal)
        setEnteredGoal('')
    }
    return  (
        <Modal visible={props.visible} animationType='slide'>
            <View style={styles.inputContainer}>
                <TextInput 
                    placeholder='Course Goal' 
                    style={styles.input}
                    onChangeText={goalInputHandler}
                    value={enteredGoal}
                />
                <View style={styles.buttonsContainer}>
                    <Button title='ADD' onPress={addGoalHandler} />
                    <Button title='CANCEL' color='red' onPress={props.onCancel} />
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    inputContainer:{
        flex:1,
        justifyContent:'center', 
        alignItems:'center'
      },
      input: {
        width: '80%', 
        borderBottomColor:'black', 
        borderBottomWidth:1, 
        padding:10
      },
      buttonsContainer: {
          flexDirection:'row',
          justifyContent:'center',
          alignItems:'center'

      }
})

export default GoalInput;