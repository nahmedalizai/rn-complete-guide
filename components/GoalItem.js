import React from 'react'
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native'

const GoalItem = (props) => {
    return <TouchableOpacity onPress={props.onDelete.bind(this, props.id)}> 
                <View style={styles.listItem}>
                    <Text>{props.value}</Text>
                </View>
            </TouchableOpacity>
}

const styles = StyleSheet.create({
    listItem: {
        padding:10,
        marginVertical: 5,
        backgroundColor: '#f8f8ff',
        borderColor: 'black',
        borderWidth:1
      }
})

export default GoalItem;