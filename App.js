import React, {useState} from 'react';
import { StyleSheet, View, Button, FlatList } from 'react-native';
import GoalItem from './components/GoalItem';
import GoalInput from './components/GoalInput';

export default function App() {
  const [courseGoals, setCourseGoals] = useState([]);
  const [isInputMode, setIsInputMode] = useState(false);
  const addGoalButtonHandler = goalValue => {
    //setCourseGoals([...courseGoals, enteredGoal]); //valid
    //setCourseGoals(currentGoals => {return [...currentGoals, enteredGoal]}); // shorter syntax below
    setCourseGoals(currentGoals => [...currentGoals, {
      id: Math.random().toString(),
      value: goalValue
    }]); //same as second
    setIsInputMode(false) //to close Modal
  };

  const removeGoalHandler = goalId => {
    setCourseGoals(currentGoals => {
      return currentGoals.filter((goal) => goal.id !== goalId )
    })
  }

  const cancelGoalAdditionHandler = () => {
    setIsInputMode(false)
  }
  return (
    <View style={styles.container}>
      <Button title='Add New Goal' onPress={() => setIsInputMode(true)} />
      <GoalInput visible={isInputMode} addGoal={addGoalButtonHandler} onCancel={cancelGoalAdditionHandler}/>
      {/* <ScrollView>
        {courseGoals.map((goal, index) => <View  key={index} style={styles.listItem}><Text>{goal}</Text></View>)}
      </ScrollView> */}
      <FlatList
        keyExtractor={(item, index) => item.id} // get key from object array
        data={courseGoals}
        renderItem={itemData => <GoalItem id={itemData.item.id} value={itemData.item.value} onDelete={removeGoalHandler}/>}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding:50
  }
});
